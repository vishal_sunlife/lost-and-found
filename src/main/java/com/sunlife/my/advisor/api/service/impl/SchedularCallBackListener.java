package com.sunlife.my.advisor.api.service.impl;

import java.util.Date;

import org.springframework.stereotype.Service;

import com.sunlife.my.advisor.api.service.SchedularCompletionCallback;

@Service
public class SchedularCallBackListener implements SchedularCompletionCallback {

	@Override
	public void onTaskComplete() {
		// TODO Auto-generated method stub
		System.out.println("Operation completed at " + new Date().toString());
	}

}
