package com.sunlife.my.advisor.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sunlife.my.advisor.api.DTO.ApiResponse;
import com.sunlife.my.advisor.api.DTO.ItemUploadRequest;
import com.sunlife.my.advisor.api.service.ItemUploadService;
import com.sunlife.my.advisor.api.service.SampleService;

@RestController
public class UploadController {

	@Autowired
	ItemUploadService itemUploadService;

	@RequestMapping(value = "/uploadItem", method = RequestMethod.POST)
	public ResponseEntity<ApiResponse> uploadItem(@RequestBody ItemUploadRequest request) {

		ApiResponse response = itemUploadService.getAdvisorDetails(request);
		return new ResponseEntity<ApiResponse>(response, HttpStatus.valueOf(response.getApiCode()));

	}
	
	@RequestMapping(value = "/claimItem", method = RequestMethod.GET)
	public ResponseEntity<ApiResponse> claimItem(@RequestParam String advisorCode, @RequestParam String itemId) {

		ApiResponse response = itemUploadService.claimItem(advisorCode, itemId);
		return new ResponseEntity<ApiResponse>(response, HttpStatus.valueOf(response.getApiCode()));

	}
	
	@RequestMapping(value = "/getAllItems", method = RequestMethod.GET)
	public ResponseEntity<ApiResponse> getItemList() {

		ApiResponse response = itemUploadService.getItemList();
		return new ResponseEntity<ApiResponse>(response, HttpStatus.valueOf(response.getApiCode()));

	}

	@RequestMapping(value = "/pointsAwarded", method = RequestMethod.GET)
	public ResponseEntity<ApiResponse> getPointsAwarded(@RequestParam String advisorCode) {

		ApiResponse response = itemUploadService.getPointsAwarded(advisorCode);
		return new ResponseEntity<ApiResponse>(response, HttpStatus.valueOf(response.getApiCode()));

	}

}
