package com.sunlife.my.advisor.api.DTO;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AgentResponse implements Serializable {

	private static final long serialVersionUID = 6777353370110397255L;

	private String name;
	private String advisorCode;
	private String emailId;
	private String mobileNo;
	private String agentType;

}
