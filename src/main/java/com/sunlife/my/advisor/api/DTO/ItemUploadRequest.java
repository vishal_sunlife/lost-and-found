package com.sunlife.my.advisor.api.DTO;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemUploadRequest implements Serializable {

	private static final long serialVersionUID = 6777353370110397255L;

	private String advisorCode;
	private String description;
	private String category;
	private String imageBytes;

}
