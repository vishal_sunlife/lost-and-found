package com.sunlife.my.advisor.api.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sunlife.my.advisor.api.model.Agent;
import com.sunlife.my.advisor.api.model.Items;

public interface ItemRepository extends JpaRepository<Items, Long> {
	
	@Query("SELECT ti from Items ti where ti.itemId=:itemId")
	public Items getByItemId(@Param("itemId") Long itemId);

	@Query("SELECT ti from Items ti where ti.claimed= 'N'")
	public ArrayList<Items> getAllItems();
}