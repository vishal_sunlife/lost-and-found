package com.sunlife.my.advisor.api.exception;

public class MyAdvisorAPIException extends Throwable {

	private static final long serialVersionUID = -8303150272401904300L;

	private String errorCode1;

	private String parameters;

	private String applicableField;

	private String message;

	
	public MyAdvisorAPIException(MYAppExceptionCodes exceptionCode) {
		this.errorCode1 = exceptionCode.getId();
		this.message =exceptionCode.getMsg();
	}
	
	public MyAdvisorAPIException(String errorCode, String message) {
		this.errorCode1 = errorCode;
		this.message = message;
	}

	public String getErrorCode() {
		return errorCode1;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode1 = errorCode;
	}

	public String getParameters() {
		return parameters;
	}

	public void setParameters(String parameters) {
		this.parameters = parameters;
	}

	public String getApplicableField() {
		return applicableField;
	}

	public void setApplicableField(String applicableField) {
		this.applicableField = applicableField;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
