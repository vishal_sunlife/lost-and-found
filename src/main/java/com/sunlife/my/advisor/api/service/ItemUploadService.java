package com.sunlife.my.advisor.api.service;

import com.sunlife.my.advisor.api.DTO.ApiResponse;
import com.sunlife.my.advisor.api.DTO.ItemUploadRequest;

public interface ItemUploadService {

	ApiResponse getAdvisorDetails(ItemUploadRequest request);

	ApiResponse claimItem(String advisorCode, String itemId);

	ApiResponse getItemList();

	ApiResponse getPointsAwarded(String advisorCode);

}
