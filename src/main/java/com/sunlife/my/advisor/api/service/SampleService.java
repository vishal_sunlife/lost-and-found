package com.sunlife.my.advisor.api.service;

import com.sunlife.my.advisor.api.DTO.ApiResponse;

public interface SampleService {

	ApiResponse getAdvisorDetails(String advisorCode);
	
	
}
