package com.sunlife.my.advisor.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sunlife.my.advisor.api.DTO.ApiResponse;
import com.sunlife.my.advisor.api.service.SampleService;

@RestController
public class SampleController {

	@Autowired
	SampleService sampleService;

	@RequestMapping(value = "/getAdvisorDetails", method = RequestMethod.GET)
	public ResponseEntity<ApiResponse> sendNotification(@RequestParam String advisorCode) {

		ApiResponse response = sampleService.getAdvisorDetails(advisorCode);
		return new ResponseEntity<ApiResponse>(response, HttpStatus.valueOf(response.getApiCode()));

	}
	

}
