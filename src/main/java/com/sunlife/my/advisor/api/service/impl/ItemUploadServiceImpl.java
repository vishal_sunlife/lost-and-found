package com.sunlife.my.advisor.api.service.impl;

import java.util.ArrayList;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.sunlife.my.advisor.api.DTO.AgentResponse;
import com.sunlife.my.advisor.api.DTO.ApiResponse;
import com.sunlife.my.advisor.api.DTO.Item;
import com.sunlife.my.advisor.api.DTO.ItemUploadRequest;
import com.sunlife.my.advisor.api.DTO.ItemsListResponse;
import com.sunlife.my.advisor.api.model.Agent;
import com.sunlife.my.advisor.api.model.Items;
import com.sunlife.my.advisor.api.model.Points;
import com.sunlife.my.advisor.api.repository.AgentInfoRepository;
import com.sunlife.my.advisor.api.repository.ClaimsRepository;
import com.sunlife.my.advisor.api.repository.ItemRepository;
import com.sunlife.my.advisor.api.service.ItemUploadService;
import com.sunlife.my.advisor.api.service.SampleService;
import com.sunlife.my.advisor.api.util.MYApiConstants;
import com.sunlife.my.advisor.api.util.Utilities;

@Service
public class ItemUploadServiceImpl implements ItemUploadService {

	private final Logger logger = LoggerFactory.getLogger(ItemUploadServiceImpl.class);
	
	@Autowired
	AgentInfoRepository agentInfoRepository;
	
	@Autowired
	ItemRepository itemRepository;
	
	@Autowired
	ClaimsRepository claimRepository;

	@Override
	public ApiResponse getAdvisorDetails(ItemUploadRequest request) {
		ApiResponse response = new ApiResponse();		
		try {
			if(checkRequest(request)) {
				Agent agent = agentInfoRepository.getByAgentCode(request.getAdvisorCode());
				if(agent != null) {
					Items item = new Items();
					item.setCategory(request.getCategory());
					item.setCreatedDate(new Date());
					item.setLastUpdatedDate(new Date());
					item.setDescription(request.getDescription());
					item.setImageBytes(request.getImageBytes());
					item.setFinderAgent(agent);
					item.setClaimed("N");
					itemRepository.save(item);
					
					response.setApiCode(HttpStatus.OK.value());
					response.setMessage(MYApiConstants.ITEM_UPLOADED);
					response.setSuccessful(true);
					
				} else {
					response.setApiCode(HttpStatus.BAD_REQUEST.value());
					response.setMessage(MYApiConstants.ADVISOR_NOT_FOUND);
					response.setSuccessful(false);
				}		
			} else {
				response.setApiCode(HttpStatus.BAD_REQUEST.value());
				response.setSuccessful(false);
				response.setMessage(MYApiConstants.INVALID_REQUEST);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setApiCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.setMessage(MYApiConstants.INTERNAL_SERVER_ERROR);
		}
		
		return response;
	}

	private boolean checkRequest(ItemUploadRequest request) {
		if(!Utilities.isNullOrEmpty(request.getAdvisorCode()) && !Utilities.isNullOrEmpty(request.getCategory()) && !Utilities.isNullOrEmpty(request.getDescription()) && !Utilities.isNullOrEmpty(request.getImageBytes())) {
			return true;
		} else {
			return false;
		}		
	}

	@Override
	public ApiResponse claimItem(String advisorCode, String itemId) {
		ApiResponse response = new ApiResponse();
		long id = Long.parseLong(itemId);	
		
		Items item = itemRepository.getByItemId(id);
		Agent agent = agentInfoRepository.getByAgentCode(advisorCode);
		
		if(item!= null) {
			if(item.getClaimed().equals("Y")) {
				response.setApiCode(HttpStatus.BAD_REQUEST.value());
				response.setSuccessful(false);
				response.setMessage(MYApiConstants.ITEM_ALREADY_CLAIMED);
			} else {
				if(agent != null) {
					if(agent.getAgentid() == item.getFinderAgent().getAgentid()) {
						
						response.setApiCode(HttpStatus.BAD_REQUEST.value());
						response.setMessage(MYApiConstants.CLAIMANT_ID_EQUAL_UPLOADER);
						response.setSuccessful(false);
					} else {
						Points claims = new Points();
						claims.setCreatedDate(new Date());
						claims.setFinderAgent(item.getFinderAgent());
						claims.setLastUpdatedDate(new Date());
						claims.setReceiverAgent(agent);
						claims.setPointsAwarded(10);
						
						claimRepository.save(claims);
						response.setApiCode(HttpStatus.OK.value());
						response.setMessage(MYApiConstants.CLAIMED_SUCCESS);
						response.setSuccessful(true);
						
						item.setClaimed("Y");
						itemRepository.save(item);
					}
					
					
					
				} else {
					response.setApiCode(HttpStatus.BAD_REQUEST.value());
					response.setMessage(MYApiConstants.ADVISOR_NOT_FOUND);
					response.setSuccessful(false);
				}
			}
		}
		return response;
	}

	@Override
	public ApiResponse getItemList() {
		ApiResponse response = new ApiResponse();
		
		ArrayList<Items> itemListFromDb = itemRepository.getAllItems();
		
		if(itemListFromDb != null && itemListFromDb.size()>0) {
		//	ItemsListResponse itemListResponse = new ItemsListResponse();
			ArrayList<Item> itemsList = new ArrayList<>();
			for(Items itemFromDB : itemListFromDb) {
				Item item = new Item();
				item.setCategory(itemFromDB.getCategory());
				item.setDescription(itemFromDB.getDescription());
				item.setItemId(itemFromDB.getItemId().toString());
				item.setUploadedOn(itemFromDB.getCreatedDate().toString());
				itemsList.add(item);
				
			}
	//		itemListResponse.setItemList(itemsList);
			response.setData(itemsList);
			
			response.setApiCode(HttpStatus.OK.value());
			response.setMessage(MYApiConstants.SUCCESS);
			response.setSuccessful(true);
			
		} else {
			response.setApiCode(HttpStatus.OK.value());
			response.setMessage(MYApiConstants.NO_ITEMS);
			response.setSuccessful(true);
		}
		
		
		return response;
	}

	@Override
	public ApiResponse getPointsAwarded(String advisorCode) {
		ApiResponse response = new ApiResponse();
		
		Agent agent = agentInfoRepository.getByAgentCode(advisorCode);
		
		if(agent != null) {
			int points = claimRepository.getPoints(advisorCode);
			response.setData(points);
			response.setApiCode(HttpStatus.OK.value());
			response.setMessage(MYApiConstants.SUCCESS);
			response.setSuccessful(true);
			
		} else {
			response.setApiCode(HttpStatus.BAD_REQUEST.value());
			response.setMessage(MYApiConstants.ADVISOR_NOT_FOUND);
			response.setSuccessful(false);
		}
		
		return response;
	}
	
	

}
