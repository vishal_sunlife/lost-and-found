package com.sunlife.my.advisor.api.DTO;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApiResponse implements Serializable {

	private static final long serialVersionUID = 6777353370110397255L;

	private boolean isSuccessful;
	private String message;
	private int apiCode;
	private Object data;

}
