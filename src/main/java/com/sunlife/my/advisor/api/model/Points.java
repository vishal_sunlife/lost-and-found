package com.sunlife.my.advisor.api.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
@Table(name = "Points")
public class Points {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="FINDER_AGENT_CODE")
	private Agent finderAgent;
	

	@ManyToOne
	@JoinColumn(name="RECEIVER_AGENT_CODE")
	private Agent receiverAgent;
	
	@Column(name = "POINTS_AWARDED")
	private int pointsAwarded;
	

	@Column(name = "CREATED_DATE", nullable = false)
	private Date createdDate;
		
	@Column(name = "LAST_UPDATED_DATE")
	private Date lastUpdatedDate;
	
	
}