package com.sunlife.my.advisor.api.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.sunlife.my.advisor.api.DTO.AgentResponse;
import com.sunlife.my.advisor.api.DTO.ApiResponse;
import com.sunlife.my.advisor.api.model.Agent;
import com.sunlife.my.advisor.api.repository.AgentInfoRepository;
import com.sunlife.my.advisor.api.service.SampleService;
import com.sunlife.my.advisor.api.util.MYApiConstants;
import com.sunlife.my.advisor.api.util.Utilities;

@Service
public class SampleServiceImpl implements SampleService {

	private final Logger logger = LoggerFactory.getLogger(SampleServiceImpl.class);
	
	@Autowired
	AgentInfoRepository agentInfoRepository;
	
	@Override
	public ApiResponse getAdvisorDetails(String advisorCode) {
		ApiResponse response = new ApiResponse();
		try {
			if(Utilities.isNullOrEmpty(advisorCode)) {
				response.setApiCode(HttpStatus.BAD_REQUEST.value());
				response.setMessage(MYApiConstants.INVALID_REQUEST);
			} else {
				response.setSuccessful(true);
				response.setApiCode(HttpStatus.OK.value());
				Agent agent = agentInfoRepository.getByAgentCode(advisorCode);
				if(agent != null) {
					AgentResponse agentResponse = new AgentResponse();
					agentResponse.setAdvisorCode(agent.getAgentCode());
					agentResponse.setEmailId(agent.getEmailId());
					agentResponse.setMobileNo(agent.getMobileNo());
					agentResponse.setAgentType(agent.getAgentType());
					agentResponse.setName(agent.getName());
					response.setData(agentResponse);
					response.setMessage(MYApiConstants.ADVISOR_FOUND);
				} else {
					response.setApiCode(HttpStatus.OK.value());
					response.setMessage(MYApiConstants.ADVISOR_NOT_FOUND);
				}
				
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setApiCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.setMessage(MYApiConstants.INTERNAL_SERVER_ERROR);
		}

		return response;
	}

}
