package com.sunlife.my.advisor.api.exception;

public enum MYAppExceptionCodes {

	INVALID_TOKEN("invalid_token", "Token is invalid"), TOKEN_EXPIRED("expired_token", "Token is expired"),
	USER_ALREADY_REGISTERED_TRAINING("MY-APP-API-003", "User is already register in the training"),
	MISSING_MANDATORY_PARAMETER("MY-APP-API-004", "request missing mandatory parameter"),
	SEAT_NOT_AVAILABLE("MY-APP-API-005", "Seat not available for training schedule"),
	AGENT_NOT_FOUND("MY-APP-API-006", "Agent Missing"),
	TRAINING_NOT_FOUND("MY-APP-API-007", "Training No longer valid/Not Found"),
	NO_MORE_DATA_TO_SHOW_PAGINATION("MY-APP-API-008", "NO_MORE_DATA_TO_SHOW_PAGINATION"),
	CLASSROOM_TRAINING_SESSION_ID_MISSING("MY-APP-API-009",
			"Classroom Training Registration shall have a training ID associated"),
	USER_NOT_REGISTERED("MY-APP-API-010", "User is not registered into the training"),
	TRAINING_MARKED_COMPLETE("MY-APP-API-010", "Training is Complete or past start date"),
	NO_ASSESSMENT_FOUND("MY-APP-API-011","No Assessment found"),
	WRONG_QUESTION("MY-APP-API-012","The Question doesn't match"),
	TRAINING_IS_PASSED("MY-APP-API-013","The Training is passed"),
	USER_ALREADY_CANCELLED_TRAINING("MY-APP-API-014", "User is already cancelled  the training"),
	INVALID_REQUEST("MY-APP-API-015", "Invalid Request"),
	NO_DATA_FOUND("MY-APP-API-016", "No Records Found"),
	INTERNAL_ERROR("MY-APP-API-017", "Internal Error while connecting db");
	

	private final String id;
	private final String msg;

	private MYAppExceptionCodes(String id, String msg) {
		this.id = id;
		this.msg = msg;
	}

	public String getId() {
		return id;
	}

	public String getMsg() {
		return msg;
	}

}
