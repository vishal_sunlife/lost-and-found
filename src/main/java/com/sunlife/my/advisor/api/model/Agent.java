package com.sunlife.my.advisor.api.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
@Table(name = "AGENT_INFO_SAMPLE")
public class Agent {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "AGENT_ID")
	private Long agentid;

	@Column(name = "AGENT_CODE", nullable = false)
	private String agentCode;

	@Column(name = "CREATED_DATE", nullable = false)
	private Date createdDate;
	
	@Column(name = "CLIENT_ID")
	private String clientId;
	
	@Column(name = "EMAIL_ID")
	private String emailId;
	
	@Column(name = "MOBILE_NO")
	private String mobileNo;
	
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "AGENT_TYPE")
	private String agentType;

	
	@Column(name = "LAST_UPDATED_DATE")
	private Date lastUpdatedDate;
	
	
}