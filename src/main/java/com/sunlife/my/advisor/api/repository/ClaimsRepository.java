package com.sunlife.my.advisor.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sunlife.my.advisor.api.model.Agent;
import com.sunlife.my.advisor.api.model.Items;
import com.sunlife.my.advisor.api.model.Points;

public interface ClaimsRepository extends JpaRepository<Points, Long> {
	
	@Query("SELECT COALESCE(SUM(ti.pointsAwarded),0) as SUM from Points ti where ti.finderAgent.agentCode =:agentCode")
	public int getPoints(@Param("agentCode") String agentCode);

}