package com.sunlife.my.advisor.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sunlife.my.advisor.api.model.Agent;

public interface AgentInfoRepository extends JpaRepository<Agent, Long> {
	
	@Query("SELECT ti from Agent ti where ti.agentCode=:agentCode")
	public Agent getByAgentCode(@Param("agentCode") String agentCode);

}