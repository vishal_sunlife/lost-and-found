package com.sunlife.my.advisor.api.util;

public class MYApiConstants {

    public static final String INTERNAL_SERVER_ERROR = "Something went wrong, please try again later";
    public static final String INVALID_REQUEST = "Invalid request, missing parameters";
	public static final String ADVISOR_NOT_FOUND = "Advisor not found";
	public static final String ADVISOR_FOUND = "Advisor found";
	public static final String ITEM_UPLOADED = "Item uploaded";
	public static final String ITEM_ALREADY_CLAIMED = "Item already claimed";
	public static final String CLAIMED_SUCCESS = "Claimed Success";
	public static final String CLAIMANT_ID_EQUAL_UPLOADER = "You cannot claim your own uploaded item";
	public static final String NO_ITEMS = "No Items in the list";
	public static final String SUCCESS = "Success";

}
