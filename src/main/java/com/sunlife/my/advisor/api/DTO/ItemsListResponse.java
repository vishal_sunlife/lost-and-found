package com.sunlife.my.advisor.api.DTO;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemsListResponse implements Serializable {

	private static final long serialVersionUID = 6777353370110397255L;

	private ArrayList<Item> itemList;
	

}
