package com.sunlife.my.advisor.api.DTO;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Item implements Serializable {

	private static final long serialVersionUID = 6777353370110397255L;

	private String itemId;
	private String category;
	private String description;
	private String uploadedOn;

}
