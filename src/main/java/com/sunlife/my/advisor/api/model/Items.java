package com.sunlife.my.advisor.api.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
@Table(name = "ITEMS")
public class Items {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ITEM_ID")
	private Long itemId;
	
	@ManyToOne
	@JoinColumn(name="FINDER_AGENT_CODE")
	private Agent finderAgent;
	
	@Column(name= "DESCRIPTION")
	private String description;
	
	@Column(name= "CATEGORY")
	private String category;
	
	@Column(name= "CLAIMED")
	private String claimed;
	
	@Column(name = "ITEM_IMAGE_BYTES")
	private String imageBytes;

	@Column(name = "CREATED_DATE", nullable = false)
	private Date createdDate;
		
	@Column(name = "LAST_UPDATED_DATE")
	private Date lastUpdatedDate;
	
	
}